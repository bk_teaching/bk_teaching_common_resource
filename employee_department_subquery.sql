CREATE TABLE employee_subquery (
	id INT PRIMARY KEY AUTO_INCREMENT, 
	first_name VARCHAR(255),
	last_name VARCHAR(255),
	department_name VARCHAR(255)
);

CREATE TABLE department_subquery (
	id INT PRIMARY KEY AUTO_INCREMENT,
	department_code VARCHAR(255),
	department_name  VARCHAR(255)
);

INSERT INTO employee_subquery (id, first_name, last_name, department_name)
VALUES
	(1, 'Nguyen', 'Ngoc', 'Nhan Su'),
	(2, 'Tran', 'Nam', 'Cong Nghe Thong Tin Phan Mem'),
	(3, 'Nguyen', 'Hai', 'Nhan Su'),
	(4, 'Nguyen', 'Ngoc', 'Ke Toan');

INSERT INTO department_subquery (id, department_code, department_name)
VALUES
	(1, 'NS', 'Nhan Su'),
	(2, 'CNTT_PM', 'Cong Nghe Thong Tin Phan Mem'),
	(3, 'CNTT_PC', 'Cong Nghe Thong Tin Phan Cung'),
	(4, 'KT', 'Ke Toan'),
	(5, 'CNTP_CB', 'Cong Nghe Thuc Pham Che Bien'),
	(6, 'CNTP_NC', 'Cong Nghe Thuc Pham Nghien Cuu'),
	(7, 'Quan Ly Du An', 'QLDA');
