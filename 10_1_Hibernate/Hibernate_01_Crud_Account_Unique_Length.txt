BTVN:
	1. Tạo 1 entity Account có các trường sau:
		id
		username: Không có giá trị trùng nhau, độ dài tối đa 20, không được null
		password: Độ dài tối đa 30
		fullName: Độ dài tối đa 30
		address: Độ dài tối đa 100
		role: Là 1 trong những giá trị sau: USER, ADMIN, MANAGER, EMPLOYEE
		birthday: Lưu ngày tháng năm, giờ phút giây

	2. Viết code lấy tất cả bản ghi rồi in ra
	3. Viết code insert bản ghi
	4. Viết code update bản ghi
	5. Viết code delete bản ghi
	6. Viết code truyền vào id và in ra kết quả xem có tồn tại bản ghi đó trong db không
	7. Viết code truyền vào username và in ra kết quả xem có tồn tại bản ghi đó trong db không


	- Thời gian nộp: 8 tiếng trước buổi tiếp theo